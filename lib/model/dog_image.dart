import 'package:json_annotation/json_annotation.dart';

part 'dog_image.g.dart';

@JsonSerializable()
class DogImage {
  final String message;
  final String status;

  DogImage({
    required this.message,
    required this.status,
  });

  factory DogImage.fromJson(Map<String, dynamic> json) => _$DogImageFromJson(json);
  Map<String, dynamic> toJson() => _$DogImageToJson(this);

  DogImage copyWith({
    String? message,
    String? status
  }) {
    return DogImage(
      message: message ?? this.message,
      status: status ?? this.status
    );
  }

}
