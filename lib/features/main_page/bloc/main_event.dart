part of 'main_bloc.dart';

@immutable
abstract class MainEvent {}

class MainInitEvent extends MainEvent {}

class MainPageLoadEvent extends MainEvent {}

class MainPageFetchOther extends MainEvent {}
