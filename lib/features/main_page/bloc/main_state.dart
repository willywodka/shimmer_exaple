part of 'main_bloc.dart';

@immutable
abstract class MainState {
  final PageState pageState;

  const MainState(this.pageState);
}

class MainInitial extends MainState {
  const MainInitial(PageState pageState) : super(pageState);
}

class MainUp extends MainState {
  const MainUp(PageState pageState) : super(pageState);
}

class PageState {
  final List<DogImage> dogImage;
  final bool onLoad;

  PageState({
    this.dogImage = const [],
    this.onLoad = false,
  });

  PageState copyWith({
    List<DogImage>? dogImage,
    bool? onLoad,
  }) {
    return PageState(
      dogImage: dogImage ?? this.dogImage,
      onLoad: onLoad ?? this.onLoad,
    );
  }
}
