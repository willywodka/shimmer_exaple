import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:shimmer_example/model/dog_image.dart';
import 'package:shimmer_example/repository/dog_image_repository.dart';

part 'main_event.dart';

part 'main_state.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  final DogImageRepository dogImageRepository;

  final int imageCount = 9;

  MainBloc({
    required PageState pageState,
    required this.dogImageRepository,
  }) : super(MainInitial(pageState)) {
    on<MainEvent>((event, emit) {});
    on<MainInitEvent>(init);
    on<MainPageFetchOther>(fetchOther);
    add(MainInitEvent());
  }

  init(MainInitEvent event, emit) async {
    final List<DogImage> list = await dogImageRepository.fetchMultipleImages(imageCount);
    emit(MainUp(state.pageState.copyWith(dogImage: list)));
  }

  fetchOther(MainPageFetchOther event, emit) async {
    final List<DogImage> list = await dogImageRepository.fetchMultipleImages(imageCount);
    emit(MainUp(state.pageState.copyWith(dogImage: list)));
  }


}
