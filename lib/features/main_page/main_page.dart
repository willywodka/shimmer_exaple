import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:shimmer_example/core/widgets/network_image.dart';
import 'package:shimmer_example/features/main_page/bloc/main_bloc.dart';
import 'package:shimmer_example/repository/dog_image_repository.dart';

class MainPage extends StatefulWidget {
  const MainPage({
    super.key,
  });

  @override
  State<MainPage> createState() => MainPageState();
}

class MainPageState extends State<MainPage> {
  final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey =
      GlobalKey<LiquidPullToRefreshState>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MainBloc(
          pageState: PageState(),
          dogImageRepository: context.read<GetIt>().get<DogImageRepository>()),
      child: BlocConsumer<MainBloc, MainState>(
        listener: (context, state) {
          // TODO: implement listener
        },
        builder: (context, state) {
          return Scaffold(
            body: LiquidPullToRefresh(
              key: _refreshIndicatorKey,
              showChildOpacityTransition: false,
              onRefresh: () async {
                context.read<MainBloc>().add(MainPageFetchOther());
              },
              child: state.pageState.dogImage.isNotEmpty
                  ? GridView.count(
                      crossAxisCount: 3,
                      children: state.pageState.dogImage
                          .map(
                            (e) => ShimmerNetworkImage(
                              imageUrl: e.message,
                            ),
                          )
                          .toList())
                  // )
                  : Container(),
            ),
          );
        },
      ),
    );
  }

// Widget _buildTopRowList() {
//   return SizedBox(
//     height: 72,
//     child: ListView(
//       physics: _isLoading ? const NeverScrollableScrollPhysics() : null,
//       scrollDirection: Axis.horizontal,
//       shrinkWrap: true,
//       children: [
//         const SizedBox(width: 16),
//         _buildTopRowItem(),
//         _buildTopRowItem(),
//         _buildTopRowItem(),
//         _buildTopRowItem(),
//         _buildTopRowItem(),
//         _buildTopRowItem(),
//       ],
//     ),
//   );
// }

// Widget _buildTopRowItem() {
//   return ShimmerLoading(
//     isLoading: _isLoading,
//     child: const CircleListItem(),
//   );
// }
//
// Widget _buildListItem() {
//   return ShimmerLoading(
//     isLoading: _isLoading,
//     child: CardListItem(
//       isLoading: _isLoading,
//     ),
//   );
// }
}
