import 'package:dio/dio.dart';
import 'package:shimmer_example/model/dog_image.dart';

class DogImageRepository {

  DogImageRepository({required this.dio});
  
  final Dio dio;
  
  Future<DogImage> fetchImage() async {
    Response<dynamic> result = await dio.get('https://dog.ceo/api/breeds/image/random');
    return DogImage.fromJson(result.data);
  }

  Future<List<DogImage>> fetchMultipleImages(int imageCount) async {
    List<DogImage> result = [];
    for (int i = 0; i < imageCount;i++) {
      result.add(await fetchImage());
    }
    return result;
  }

}