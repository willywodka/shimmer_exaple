import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:shimmer_example/features/main_page/main_page.dart';
import 'package:shimmer_example/repository/dog_image_repository.dart';

void main() {
  runApp(DogApp()
      /*const MaterialApp(
      home: ExampleUiLoadingAnimation(),
      debugShowCheckedModeBanner: false,
    ),*/
      );
}

class DogApp extends StatefulWidget {
  DogApp({super.key});

  @override
  State createState() => DogAppState();
}

class DogAppState extends State<DogApp> {
  GetIt getIt = GetIt.instance;

  @override
  void initState() {
    init().then((value) => null);
    super.initState();
  }

  Future<void> init() async {
    getIt.registerLazySingleton<DogImageRepository>(() => DogImageRepository(dio: Dio()));
  }

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
        providers: [
          RepositoryProvider(create: (context) => getIt),
        ],
        child: const MaterialApp(
          home: MainPage(),
        ));
  }
}

